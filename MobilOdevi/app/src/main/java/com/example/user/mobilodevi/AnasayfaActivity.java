package com.example.user.mobilodevi;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class AnasayfaActivity  extends AppCompatActivity {


    TextView tv_hosgeldiniz,tv_adsoyad,tv_profilim,tv_cikisyap;
    Button btn_kitapAra , btn_ilanlarim , btn_ilanver , btn_mesajlarim;
    Session session;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anasayfa);

        Tanimla();




    }

    private void Tanimla() {
        session=new Session(getApplicationContext());
        tv_hosgeldiniz=findViewById(R.id.AnaSayfaTextViewHosGeldiniz);
        tv_adsoyad=findViewById(R.id.AnaSayfatextViewAdSoyad);
        tv_profilim=findViewById(R.id.AnaSayfatextViewProfilim);
        tv_cikisyap=findViewById(R.id.AnaSayfatextViewCikisYap);
        btn_kitapAra=findViewById(R.id.AnaSayfabuttonKitapAra);
        btn_ilanlarim=findViewById(R.id.AnaSayfabuttonIlanlarim);
        btn_ilanver=findViewById(R.id.AnaSayfabuttonIlanVer);
        btn_mesajlarim=findViewById(R.id.AnaSayfabuttonMesajlarım);
        tv_adsoyad.setText(session.getPrefName());

        tv_cikisyap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //BENİ HATIRLA YAPIYORUZ---------------------------------------

                session.setPrefDurum(false);
                session.setPrefID(0);
                session.setPrefUsername("");
                //-------------------------------------------------------------
                loginSayfasinaGit();
            }
        });



    }

    private void loginSayfasinaGit() {
        Intent intent = new Intent(AnasayfaActivity.this,MainActivity.class);
        startActivity(intent);

    }


}