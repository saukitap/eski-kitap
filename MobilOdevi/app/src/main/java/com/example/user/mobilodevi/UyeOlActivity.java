package com.example.user.mobilodevi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.example.user.mobilodevi.Entities.APIClient;
import com.example.user.mobilodevi.Entities.Kullanicilar;
import com.example.user.mobilodevi.Services.KullanicilarService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UyeOlActivity extends AppCompatActivity {


    EditText et_AdSoyad,et_KullaniciAdi,et_Sifre,et_SifreTekrar , et_Il , et_Ilce ,et_Mail , et_Telefon;
    Button btn_Giris,btn_Iptal;
    String adsoyad,kullanciadi,sifre,sifretekrar,il,ilce,mail,telefon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uye_ol);

        Tanimla();
    }

    private void Tanimla() {
        et_AdSoyad=findViewById(R.id.UyeOlEditTextAdSoyad);
        et_KullaniciAdi=findViewById(R.id.UyeOlEditTextKullaniciAdi);
        et_Sifre=findViewById(R.id.UyeOlEditTextSifre);
        et_SifreTekrar=findViewById(R.id.UyeOlEditTextSifreTekrar);
        et_Il=findViewById(R.id.UyeOlEditTextSehir);
        et_Ilce=findViewById(R.id.UyeOlEditTextIlce);
        et_Mail=findViewById(R.id.UyeOlEditTextMail);
        et_Telefon=findViewById(R.id.uyeOlEditTextTelefon);
        btn_Giris=findViewById(R.id.UyeOlButtonGiris);
        btn_Iptal=findViewById(R.id.UyeOlButtonIptal);

        btn_Giris.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adsoyad = et_AdSoyad.getText().toString();
                kullanciadi = et_KullaniciAdi.getText().toString();
                sifre = et_Sifre.getText().toString();
                sifretekrar = et_SifreTekrar.getText().toString();
                il = et_Il.getText().toString();
                ilce = et_Ilce.getText().toString();
                mail = et_Mail.getText().toString();
                telefon = et_Telefon.getText().toString();

                if(! (sifre.toString().equals(sifretekrar.toString())))
                {
                    Toast.makeText(UyeOlActivity.this, "Şifreler Uyuşmuyor", Toast.LENGTH_SHORT).show();

                }
                else
                {
                    UyeOlIstek(kullanciadi, sifre,adsoyad,il,ilce,mail,telefon);
                }




            }
        });
    }

    private void UyeOlIstek(String kullanciadi, String sifre, String adsoyad, String il, String ilce, String mail, String telefon)
    {

        try {
            Kullanicilar Uye_Olacak_Kullanici = new Kullanicilar();
            Uye_Olacak_Kullanici.setGuncellemeTarihi("2018-01-01");
            Uye_Olacak_Kullanici.setEposta(mail);
            Uye_Olacak_Kullanici.setAdSoyad(adsoyad);
            Uye_Olacak_Kullanici.setIlce(ilce);
            Uye_Olacak_Kullanici.setKayitTarihi("2018-01-01");
            Uye_Olacak_Kullanici.setKullaniciAdi(kullanciadi);
            Uye_Olacak_Kullanici.setRolID(2);
            Uye_Olacak_Kullanici.setSehir(il);
            Uye_Olacak_Kullanici.setTelefon(telefon);
            Uye_Olacak_Kullanici.setSifre(sifre);



            KullanicilarService kullaniciServis = APIClient.getClient().create(KullanicilarService.class);
            Call call = kullaniciServis.create(Uye_Olacak_Kullanici,APIClient.getApiSecurityUsername(),APIClient.getApiSecurityPass());
            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    if(response.isSuccessful())
                    {
                        Toast.makeText(UyeOlActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(UyeOlActivity.this,MainActivity.class);
                        startActivity(intent);

                    }
                    else
                    {

                        Toast.makeText(UyeOlActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Toast.makeText(UyeOlActivity.this, t.getMessage() , Toast.LENGTH_SHORT).show();
                }
            });


        }
        catch (Exception e)
        {

            Toast.makeText(this, e.getMessage() , Toast.LENGTH_SHORT).show();
        }


    }
}