package com.example.user.mobilodevi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class IlanVer3Activity extends AppCompatActivity {

    String durum , kitap_adi , yayin_evi,ucret , resim1 , resim2;
    Integer tur_id,kategori_id;
    EditText IlanVer3_EditText_Adres;
    Spinner ilanVer3_Spinner_Il , ilanVer3_Spinner_Ilce;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ilan_ver3);

        IlanVer3_EditText_Adres = findViewById(R.id.IlanVer3_EditText_Adres);
        ilanVer3_Spinner_Il = findViewById(R.id.ilanVer3_Spinner_Il);
        ilanVer3_Spinner_Ilce = findViewById(R.id.ilanVer3_Spinner_Ilce);

        Intent intent = getIntent();
        durum = intent.getStringExtra("intent_ilanver_durum");
        kitap_adi =  intent.getStringExtra("intent_ilanver_kitap_adi");
        yayin_evi = intent.getStringExtra("intent_ilanver_yayin_evi");
        ucret  = intent.getStringExtra("intent_ilanver_ucret");
        tur_id =  Integer.parseInt(intent.getStringExtra("intent_ilanver_tur_id"));
        kategori_id = Integer.parseInt(intent.getStringExtra("intent_ilanver_kitap_kategori_id"));
        resim1 = intent.getStringExtra("intent_ilanver_resim1");
        resim2 = intent.getStringExtra("intent_ilanver_resim2");



    }


    public void ilanverileri3(View v)
    {
        Intent intent = new Intent(IlanVer3Activity.this,IlanVer4Activity.class);
        intent.putExtra("intent_ilanver_kitap_kategori_id",""+kategori_id);
        intent.putExtra("intent_ilanver_durum",durum);
        intent.putExtra("intent_ilanver_tur_id" ,   ""+tur_id);//Duzeltilecek
        intent.putExtra("intent_ilanver_kitap_adi",kitap_adi);
        intent.putExtra("intent_ilanver_yayin_evi",yayin_evi);
        intent.putExtra("intent_ilanver_ucret",ucret);
        intent.putExtra("intent_ilanver_resim1",resim1);
        intent.putExtra("intent_ilanver_resim2",resim2);
        intent.putExtra("intent_ilanver_adres",IlanVer3_EditText_Adres.getText().toString());
        intent.putExtra("intent_ilanver_il",ilanVer3_Spinner_Il.getSelectedItem().toString());
        intent.putExtra("intent_ilanver_ilce",ilanVer3_Spinner_Ilce.getSelectedItem().toString());

        startActivity(intent);
    }

    public void ilanvergeri3(View v)
    {
       // Intent intent = new Intent(IlanVer3Activity.this,IlanVer2Activity.class);
      // startActivity(intent);

    }
}
