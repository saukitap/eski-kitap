package com.example.user.mobilodevi.Entities;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Kullanicilar{
    @SerializedName("Eposta")
    private String Eposta;
    @SerializedName("Sifre")
    private String Sifre;
    @SerializedName("AdSoyad")
    private String AdSoyad;
    @SerializedName("Ilce")
    private String Ilce;
    @SerializedName("KayitTarihi")
    private String KayitTarihi;
    @SerializedName("KullaniciAdi")
    private String KullaniciAdi;
    @SerializedName("Roller")
    private Object Roller;
    @SerializedName("Rol_ID")
    private int Rol_ID;
    @SerializedName("GuncellemeTarihi")
    private String GuncellemeTarihi;
    @SerializedName("ID")
    private int ID;
    @SerializedName("Telefon")
    private String Telefon;
    @SerializedName("Mesajlars")
    private List<Object> Mesajlars;
    @SerializedName("Sehir")
    private String Sehir;

    public void setEposta(String eposta){
        this.Eposta = eposta;
    }

    public String getEposta(){
        return Eposta;
    }

    public void setSifre(String sifre){
        this.Sifre = sifre;
    }

    public String getSifre(){
        return Sifre;
    }

    public void setAdSoyad(String adSoyad){
        this.AdSoyad = adSoyad;
    }

    public String getAdSoyad(){
        return AdSoyad;
    }

    public void setIlce(String ilce){
        this.Ilce = ilce;
    }

    public String getIlce(){
        return Ilce;
    }

    public void setKayitTarihi(String kayitTarihi){
        this.KayitTarihi = kayitTarihi;
    }

    public String getKayitTarihi(){
        return KayitTarihi;
    }

    public void setKullaniciAdi(String kullaniciAdi){
        this.KullaniciAdi = kullaniciAdi;
    }

    public String getKullaniciAdi(){
        return KullaniciAdi;
    }

    public void setRoller(Object roller){
        this.Roller = roller;
    }

    public Object getRoller(){
        return Roller;
    }

    public void setRolID(int rolID){
        this.Rol_ID = rolID;
    }

    public int getRolID(){
        return Rol_ID;
    }

    public void setGuncellemeTarihi(String guncellemeTarihi){
        this.GuncellemeTarihi = guncellemeTarihi;
    }

    public String getGuncellemeTarihi(){
        return GuncellemeTarihi;
    }

    public void setID(int iD){
        this.ID = iD;
    }

    public int getID(){
        return ID;
    }

    public void setTelefon(String telefon){
        this.Telefon = telefon;
    }

    public String getTelefon(){
        return Telefon;
    }

    public void setMesajlars(List<Object> mesajlars){
        this.Mesajlars = mesajlars;
    }

    public List<Object> getMesajlars(){
        return Mesajlars;
    }

    public void setSehir(String sehir){
        this.Sehir = sehir;
    }

    public String getSehir(){
        return Sehir;
    }

    @Override
    public String toString(){
        return
                "Kullanicilar{" +
                        "eposta = '" + Eposta + '\'' +
                        ",sifre = '" + Sifre + '\'' +
                        ",adSoyad = '" + AdSoyad + '\'' +
                        ",ilce = '" + Ilce + '\'' +
                        ",kayitTarihi = '" + KayitTarihi + '\'' +
                        ",kullaniciAdi = '" + KullaniciAdi + '\'' +
                        ",roller = '" + Roller + '\'' +
                        ",rol_ID = '" + Rol_ID + '\'' +
                        ",guncellemeTarihi = '" + GuncellemeTarihi + '\'' +
                        ",iD = '" + ID + '\'' +
                        ",telefon = '" + Telefon + '\'' +
                        ",mesajlars = '" + Mesajlars + '\'' +
                        ",sehir = '" + Sehir+ '\'' +
                        "}";
    }
}
