package com.example.user.mobilodevi.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.user.mobilodevi.R;
import com.squareup.picasso.Picasso;


public class ViewPagerAdapter extends PagerAdapter {



    Activity activity;
    String[] images;
    LayoutInflater layoutInflater;

    public ViewPagerAdapter(Activity activity,String[] images)
    {
        this.activity=activity;
        this.images=images;

    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view==o;
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) activity.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = layoutInflater.inflate(R.layout.viewpager_item,container,false);
        ImageView image = itemView.findViewById(R.id.ViewPagerImage);
        DisplayMetrics dis = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dis);
        int height = dis.heightPixels;
        int width = dis.widthPixels;
        image.setMinimumHeight(height);
        image.setMinimumWidth(width);

        try
        {
            Picasso.with(activity.getApplicationContext()).load(images[position]).placeholder(R.drawable.yukleniyor).error(R.drawable.resimbulunamadi).into(image);


        }
        catch (Exception ex){}


        container.addView(itemView);
        return itemView;


    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        ((ViewPager) container).removeView((View)object);
    }
}
