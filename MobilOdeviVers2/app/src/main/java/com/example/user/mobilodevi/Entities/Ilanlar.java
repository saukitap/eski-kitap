package com.example.user.mobilodevi.Entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Ilanlar{
	@SerializedName("Kategori")
private Object Kategori;
	@SerializedName("KitapAdi")
	private String KitapAdi;
	@SerializedName("Kategori_ID")
	private int Kategori_ID;
	@SerializedName("IlanTarihi")
	private String IlanTarihi;
	@SerializedName("Ilce")
	private String Ilce;
	@SerializedName("Ucret")
	private Object Ucret;
	@SerializedName("Tur_ID")
	private int Tur_ID;
	@SerializedName("Aciklama")
	private String Aciklama;
	@SerializedName("Turler")
	private Object Turler;
	@SerializedName("Yazar")
	private String Yazar;
	@SerializedName("Resim")
	private Object Resim;
	@SerializedName("YayinEvi")
	private String YayinEvi;
	@SerializedName("ID")
	private int ID;
	@SerializedName("Resim2")
	private Object Resim2;
	@SerializedName("Resim3")
	private Object Resim3;
	@SerializedName("Mesajlars")
	private List<Object> Mesajlars;
	@SerializedName("Kullanici_ID")
	private int Kullanici_ID;
	@SerializedName("Adres")
	private String Adres;
	@SerializedName("Sehir")
	private String Sehir;


	public void setKategori(Object Kategori){
		this.Kategori = Kategori;
	}

	public Object getKategori(){
		return Kategori;
	}

	public void setKitapAdi(String KitapAdi){
		this.KitapAdi = KitapAdi;
	}

	public String getKitapAdi(){
		return KitapAdi;
	}

	public void setKategoriID(int Kategori_ID){
		this.Kategori_ID = Kategori_ID;
	}

	public int getKategoriID(){
		return Kategori_ID;
	}

	public void setIlanTarihi(String IlanTarihi){
		this.IlanTarihi = IlanTarihi;
	}

	public String getIlanTarihi(){
		return IlanTarihi;
	}

	public void setIlce(String Ilce){
		this.Ilce = Ilce;
	}

	public String getIlce(){
		return Ilce;
	}

	public void setUcret(Object Ucret){
		this.Ucret = Ucret;
	}

	public Object getUcret(){
		return Ucret;
	}

	public void setTurID(int Tur_ID){
		this.Tur_ID = Tur_ID;
	}

	public int getTurID(){
		return Tur_ID;
	}

	public void setAciklama(String Aciklama){
		this.Aciklama = Aciklama;
	}

	public String getAciklama(){
		return Aciklama;
	}

	public void setTurler(Object Turler){
		this.Turler = Turler;
	}

	public Object getTurler(){
		return Turler;
	}

	public void setYazar(String Yazar){
		this.Yazar = Yazar;
	}

	public String getYazar(){
		return Yazar;
	}

	public void setResim(Object Resim){
		this.Resim = Resim;
	}

	public Object getResim(){
		return Resim;
	}

	public void setYayinEvi(String YayinEvi){
		this.YayinEvi = YayinEvi;
	}

	public String getYayinEvi(){
		return YayinEvi;
	}

	public void setID(int ID){
		this.ID = ID;
	}

	public int getID(){
		return ID;
	}

	public void setResim2(Object Resim2){
		this.Resim2 = Resim2;
	}

	public Object getResim2(){
		return Resim2;
	}

	public void setResim3(Object Resim3){
		this.Resim3 = Resim3;
	}

	public Object getResim3(){
		return Resim3;
	}

	public void setMesajlars(List<Object> Mesajlars){
		this.Mesajlars = Mesajlars;
	}

	public List<Object> getMesajlars(){
		return Mesajlars;
	}

	public void setKullaniciID(int Kullanici_ID){
		this.Kullanici_ID = Kullanici_ID;
	}

	public int getKullaniciID(){
		return Kullanici_ID;
	}

	public void setAdres(String Adres){
		this.Adres = Adres;
	}

	public String getAdres(){
		return Adres;
	}

	public void setSehir(String Sehir){
		this.Sehir = Sehir;
	}

	public String getSehir(){
		return Sehir;
	}

	@Override
 	public String toString(){
		return 
			"Ilanlar{" + 
			"kategori = '" + Kategori + '\'' +
			",kitapAdi = '" + KitapAdi + '\'' +
			",kategori_ID = '" + Kategori_ID + '\'' +
			",ilanTarihi = '" + IlanTarihi + '\'' +
			",ilce = '" + Ilce + '\'' +
			",ucret = '" + Ucret + '\'' +
			",tur_ID = '" + Tur_ID + '\'' +
			",aciklama = '" + Aciklama + '\'' +
			",turler = '" + Turler + '\'' +
			",yazar = '" + Yazar + '\'' +
			",resim = '" + Resim + '\'' +
			",yayinEvi = '" + YayinEvi + '\'' +
			",iD = '" + ID + '\'' +
			",resim2 = '" + Resim2 + '\'' +
			",resim3 = '" + Resim3 + '\'' +
			",mesajlars = '" + Mesajlars + '\'' +
			",kullanici_ID = '" + Kullanici_ID + '\'' +
			",adres = '" + Adres + '\'' +
			",sehir = '" + Sehir + '\'' +
			"}";
		}
}