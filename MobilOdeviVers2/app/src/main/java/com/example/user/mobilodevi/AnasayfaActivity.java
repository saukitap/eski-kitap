package com.example.user.mobilodevi;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class AnasayfaActivity  extends AppCompatActivity {


    TextView tv_hosgeldiniz,tv_adsoyad,tv_profilim,tv_cikisyap;
    Button btn_kitapAra , btn_ilanlarim , btn_ilanver , btn_mesajlarim;
    Session session;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anasayfa);

        Tanimla();




    }

    private void Tanimla() {
        session=new Session(getApplicationContext());
        tv_hosgeldiniz=findViewById(R.id.AnaSayfaTextViewHosGeldiniz);
        tv_adsoyad=findViewById(R.id.AnaSayfatextViewAdSoyad);
        tv_profilim=findViewById(R.id.AnaSayfatextViewProfilim);
        tv_cikisyap=findViewById(R.id.AnaSayfatextViewCikisYap);
        btn_kitapAra=findViewById(R.id.AnaSayfabuttonKitapAra);
        btn_ilanlarim=findViewById(R.id.AnaSayfabuttonIlanlarim);
        btn_ilanver=findViewById(R.id.AnaSayfabuttonIlanVer);
        btn_mesajlarim=findViewById(R.id.AnaSayfabuttonMesajlarım);
        tv_adsoyad.setText(session.getPrefName());


        btn_ilanver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AnasayfaActivity.this,IlanVerActivity.class);
                startActivity(intent);
            }
        });
btn_mesajlarim.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Toast.makeText(AnasayfaActivity.this, "Gelen Kutunuzda Hiç Mesaj Yok", Toast.LENGTH_SHORT).show();
    }
});


    btn_ilanlarim.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(AnasayfaActivity.this,IlanlarimActivity.class);
            startActivity(intent);
        }
    });

        tv_cikisyap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                //BENİ HATIRLAYI BOŞALTIYORUZ---------------------------------------

                                session.setPrefDurum(false);
                                session.setPrefID(0);
                                session.setPrefUsername("");
                                session.setPrefName("");
                                session.setPrefEposta("");
                                session.setPrefGuncellemeTarihi("");
                                session.setPrefIlce("");
                                session.setPrefKayitTarihi("");
                                session.setPrefRolID(0);
                                session.setPrefSehir("");
                                session.setPrefSifre("");
                                session.setPrefTelefon("");
                                //-------------------------------------------------------------
                                loginSayfasinaGit();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(AnasayfaActivity.this);
                builder.setMessage("Çıkmak İstediğinize Emin Misiniz ?").setPositiveButton("Evet", dialogClickListener)
                        .setNegativeButton("Hayır", dialogClickListener).show();






            }
        });


        tv_profilim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                profilimSayfasinaGit();
            }
        });

btn_kitapAra.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        KitapAraSayfasinaGit();
    }
});

    }



    private void KitapAraSayfasinaGit() {

        Intent intent = new Intent(AnasayfaActivity.this,KitapAraActivity.class);
        startActivity(intent);
    }

    private void loginSayfasinaGit() {
        Intent intent = new Intent(AnasayfaActivity.this,MainActivity.class);
        startActivity(intent);

    }


    private void profilimSayfasinaGit() {
        Intent intent = new Intent(AnasayfaActivity.this,ProfilimActivity.class);
        startActivity(intent);

    }


}