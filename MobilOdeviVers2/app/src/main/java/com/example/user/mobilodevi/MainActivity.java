package com.example.user.mobilodevi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.example.user.mobilodevi.Entities.APIClient;
import com.example.user.mobilodevi.Entities.Kullanicilar;
import com.example.user.mobilodevi.Services.KullanicilarService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private Session session;
    EditText et_kullaniciAdi , et_sifre;
    TextView tv_kaydol,tv_sifremiUnuttum;
    Button btn_giris;
    Kullanicilar gecerliKullanici;
    ProgressDialog progress ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        KullaniciGirisYapmisMi();





    }

    private void KullaniciGirisYapmisMi() {
        session = new Session(getApplicationContext());

        if(session.getPrefDurum())
        {
            AnasayfayaGit();
        }
        else
        {
            Tanimla();
        }


    }

    private void AnasayfayaGit() {

        Intent intent = new Intent(MainActivity.this,AnasayfaActivity.class);
        startActivity(intent);
    }

    private void Tanimla() {

        progress = new ProgressDialog(MainActivity.this);

        progress.setTitle("Lütfen Bekleyiniz");
        progress.setMessage("Giriş Yapılıyor...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        et_kullaniciAdi=findViewById(R.id.LogineditTextKullaniciAdi);
        et_sifre=findViewById(R.id.LogineditTextSifre);
        tv_kaydol=findViewById(R.id.LogintextViewKaydol);
        tv_sifremiUnuttum=findViewById(R.id.LogintextViewSifremiUnuttum);
        btn_giris=findViewById(R.id.LoginButtonGiris);

        btn_giris.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String kullaniciAdi = et_kullaniciAdi.getText().toString();
                String sifre = et_sifre.getText().toString();


                if(kullaniciAdi.equals("tumkullanicilar")  && sifre.equals("tumkullanicilar"))
                {
                    Intent intent = new Intent(MainActivity.this,TumKullanicilarActivity.class);
                    startActivity(intent);
                }
                else {

                    LoginIstek(kullaniciAdi, sifre);
                }

            }
        });

        tv_kaydol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uyeOlSayfasinaGit();
            }
        });



    }

    private void uyeOlSayfasinaGit() {

        Intent intent = new Intent(MainActivity.this,UyeOlActivity.class);
        startActivity(intent);

    }

    private void LoginIstek(final String kullaniciAdi , final String sifre)
    {
        KullanicilarService productSevice = APIClient.getClient().create(KullanicilarService.class);
        final Call<Kullanicilar> gelen_kullanici = productSevice.login(kullaniciAdi,sifre,APIClient.getApiSecurityUsername() , APIClient.getApiSecurityPass());



        progress.show();

        gelen_kullanici.enqueue(new Callback<Kullanicilar>() {
            @Override
            public void onResponse(Call<Kullanicilar> call, Response<Kullanicilar> response) {
                if(response.isSuccessful())
                {
                    if(response.body() == null)
                    {
                        Toast.makeText(MainActivity.this, "Kullanıcı Adı Veya Şifre Hatalı", Toast.LENGTH_SHORT).show();
                        progress.dismiss();

                    }
                    else
                    {
                        Toast.makeText(MainActivity.this, "Hoşgeldiniz", Toast.LENGTH_SHORT).show();

                        //BENİ HATIRLA YAPIYORUZ---------------------------------------
                        session.setPrefDurum(true);
                        session.setPrefID(response.body().getID());
                        session.setPrefUsername(response.body().getKullaniciAdi());
                        session.setPrefName(response.body().getAdSoyad());
                        session.setPrefEposta(response.body().getEposta());
                        session.setPrefGuncellemeTarihi(response.body().getGuncellemeTarihi());
                        session.setPrefIlce(response.body().getIlce());
                        session.setPrefKayitTarihi(response.body().getKayitTarihi());
                        session.setPrefRolID(response.body().getRolID());
                        session.setPrefSehir(response.body().getSehir());
                        session.setPrefSifre(response.body().getSifre());
                        session.setPrefTelefon(response.body().getTelefon());
                        //-------------------------------------------------------------
                        progress.dismiss();

                        AnasayfayaGit();
                    }




                }

                else
                {
                    progress.dismiss();
                    Toast.makeText(MainActivity.this, "Bağlantı Hatası Lütfen İnternet Bağlantınızın Açık Olduğundan Emin Olun", Toast.LENGTH_SHORT).show();


                }

            }

            @Override
            public void onFailure(Call<Kullanicilar> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(MainActivity.this, "Server Bakımda Özür Dileriz", Toast.LENGTH_SHORT).show();
            }
        });












    }
}
