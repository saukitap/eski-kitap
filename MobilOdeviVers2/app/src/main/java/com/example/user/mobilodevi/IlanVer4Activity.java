package com.example.user.mobilodevi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class IlanVer4Activity extends AppCompatActivity {

    String durum , kitap_adi , yayin_evi,ucret , resim1 , resim2,adres,il,ilce,aciklama;
    Integer tur_id,kategori_id;
    EditText et_aciklama;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ilan_ver4);

          et_aciklama=findViewById(R.id.IlanVer4EditTextAciklama);
        Intent intent = getIntent();
        durum = intent.getStringExtra("intent_ilanver_durum");
        kitap_adi =  intent.getStringExtra("intent_ilanver_kitap_adi");
        yayin_evi = intent.getStringExtra("intent_ilanver_yayin_evi");
        ucret  = intent.getStringExtra("intent_ilanver_ucret");
        tur_id =  Integer.parseInt(intent.getStringExtra("intent_ilanver_tur_id"));
        kategori_id = Integer.parseInt(intent.getStringExtra("intent_ilanver_kitap_kategori_id"));
        resim1 = intent.getStringExtra("intent_ilanver_resim1");
        resim2 = intent.getStringExtra("intent_ilanver_resim2");
        adres = intent.getStringExtra("intent_ilanver_adres");
        il = intent.getStringExtra("intent_ilanver_il");
        ilce = intent.getStringExtra("intent_ilanver_ilce");


    }

    public void ilanverileri4(View v)
    {
        Intent intent = new Intent(IlanVer4Activity.this,IlanVer5Activity.class);
        intent.putExtra("intent_ilanver_kitap_kategori_id",""+kategori_id);
        intent.putExtra("intent_ilanver_durum",durum);
        intent.putExtra("intent_ilanver_tur_id" ,   ""+tur_id);//Duzeltilecek
        intent.putExtra("intent_ilanver_kitap_adi",kitap_adi);
        intent.putExtra("intent_ilanver_yayin_evi",yayin_evi);
        intent.putExtra("intent_ilanver_ucret",ucret);
        intent.putExtra("intent_ilanver_resim1",resim1);
        intent.putExtra("intent_ilanver_resim2",resim2);
        intent.putExtra("intent_ilanver_adres",adres);
        intent.putExtra("intent_ilanver_il",il);
        intent.putExtra("intent_ilanver_ilce",ilce);
        intent.putExtra("intent_ilanver_aciklama",et_aciklama.getText().toString());

        startActivity(intent);
    }

    public void ilanvergeri4(View v)
    {
       // Intent intent = new Intent(IlanVer4Activity.this,IlanVer3Activity.class);
      //  startActivity(intent);

    }
}
