package com.example.user.mobilodevi;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.user.mobilodevi.Entities.APIClient;
import com.example.user.mobilodevi.Entities.Kullanicilar;
import com.example.user.mobilodevi.Services.KullanicilarService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfilimActivity extends AppCompatActivity {


    EditText et_AdSoyad,et_KullaniciAdi,et_Sifre,et_SifreTekrar , et_Il , et_Ilce ,et_Mail , et_Telefon;
    Button btn_Guncelle,btn_Iptal,btn_Sil;
    String adsoyad,kullanciadi,sifre,sifretekrar,il,ilce,mail,telefon;
    Session session;
    ProgressDialog progress ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profilim);

        Tanimla();
        BaslangicDegerLeriniAta();
    }



    private void Tanimla() {

        session = new Session(getApplicationContext());
        et_AdSoyad=findViewById(R.id.ProfilimEditTextAdSoyad);
        et_KullaniciAdi=findViewById(R.id.ProfilimEditTextKullaniciAdi);
        et_Sifre=findViewById(R.id.ProfilimEditTextSifre);
        et_SifreTekrar=findViewById(R.id.ProfilimEditTextSifreTekrar);
        et_Il=findViewById(R.id.ProfilimEditTextSehir);
        et_Ilce=findViewById(R.id.ProfilimEditTextIlce);
        et_Mail=findViewById(R.id.ProfilimEditTextMail);
        et_Telefon=findViewById(R.id.ProfilimEditTextTelefon);
        btn_Guncelle=findViewById(R.id.profilimButtonGuncelle);
        btn_Sil=findViewById(R.id.profilimButtonSil);
        btn_Iptal=findViewById(R.id.ProfilimButtonIptal);
        progress = new ProgressDialog(ProfilimActivity.this);
        progress.setTitle("Lütfen Bekleyiniz");
        progress.setMessage("İşlem Yapılıyor ...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog

        btn_Guncelle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adsoyad = et_AdSoyad.getText().toString();
                kullanciadi = et_KullaniciAdi.getText().toString();
                sifre = et_Sifre.getText().toString();
                sifretekrar = et_SifreTekrar.getText().toString();
                il = et_Il.getText().toString();
                ilce = et_Ilce.getText().toString();
                mail = et_Mail.getText().toString();
                telefon = et_Telefon.getText().toString();

                if(! (sifre.toString().equals(sifretekrar.toString())))
                {
                    Toast.makeText(ProfilimActivity.this, "Şifreler Uyuşmuyor", Toast.LENGTH_SHORT).show();

                }
                else
                {
                    GuncelleIstek();
                }




            }
        });


        btn_Iptal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfilimActivity.this,AnasayfaActivity.class);
                startActivity(intent);
            }
        });


        btn_Sil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                SilIstek();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(ProfilimActivity.this);
                builder.setMessage("Profilinizi Silmek  İstediğinize Emin Misiniz ?").setPositiveButton("Evet", dialogClickListener)
                        .setNegativeButton("Hayır", dialogClickListener).show();
            }
        });
    }

    public void BaslangicDegerLeriniAta()
    {
        et_AdSoyad.setText(session.getPrefName().toString());
        et_KullaniciAdi.setText(session.getPrefUsername().toString());
        et_Sifre.setText(session.getPrefSifre().toString());
        et_SifreTekrar.setText(session.getPrefSifre().toString());
        et_Il.setText(session.getPrefSehir().toString());
        et_Ilce.setText(session.getPrefIlce().toString());
        et_Mail.setText(session.getPrefEposta().toString());
        et_Telefon.setText(session.getPrefTelefon().toString());


    }

    public void GuncelleIstek()
    {


        try {
        Kullanicilar Uye_Olacak_Kullanici = new Kullanicilar();
        Uye_Olacak_Kullanici.setID(session.getPrefID());
        Uye_Olacak_Kullanici.setGuncellemeTarihi("2018-01-01");
        Uye_Olacak_Kullanici.setEposta(mail);
        Uye_Olacak_Kullanici.setAdSoyad(adsoyad);
        Uye_Olacak_Kullanici.setIlce(ilce);
        Uye_Olacak_Kullanici.setKayitTarihi("2018-01-01");
        Uye_Olacak_Kullanici.setKullaniciAdi(kullanciadi);
        Uye_Olacak_Kullanici.setRolID(2);
        Uye_Olacak_Kullanici.setSehir(il);
        Uye_Olacak_Kullanici.setTelefon(telefon);
        Uye_Olacak_Kullanici.setSifre(sifre);






progress.show();
            KullanicilarService kullanicilarService = APIClient.getClient().create(KullanicilarService.class);
            Call call = kullanicilarService.update(Uye_Olacak_Kullanici,APIClient.getApiSecurityUsername(),APIClient.getApiSecurityPass());
            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    if(response.isSuccessful())
                    {
                        progress.cancel();
                        Toast.makeText(ProfilimActivity.this, "Güncelleme Başarılı", Toast.LENGTH_SHORT).show();


                        session.setPrefUsername(kullanciadi);
                        session.setPrefName(adsoyad);
                        session.setPrefEposta(mail);
                     //   session.setPrefGuncellemeTarihi();
                        session.setPrefIlce(ilce);
                        session.setPrefSehir(il);
                        session.setPrefSifre(sifre);
                        session.setPrefTelefon(telefon);
                        //-------------------------------------------------------------

                        Intent intent = new Intent(ProfilimActivity.this,MainActivity.class);
                        startActivity(intent);

                    }
                    else
                    {
                        progress.cancel();
                        Toast.makeText(ProfilimActivity.this, "Başarısız", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    progress.cancel();
                    Toast.makeText(ProfilimActivity.this, t.getMessage() , Toast.LENGTH_SHORT).show();
                }
            });







        }
        catch (Exception e)
        {

            Toast.makeText(this, e.getMessage() , Toast.LENGTH_SHORT).show();
        }


    }


    public void SilIstek()
    {

        try {

                    KullanicilarService kullaniciService = APIClient.getClient().create(KullanicilarService.class);
                    Call call = kullaniciService.delete(session.getPrefID(),APIClient.getApiSecurityUsername(),APIClient.getApiSecurityPass());


                    progress.show();
                    call.enqueue(new Callback() {
                        @Override
                        public void onResponse(Call call, Response response) {
                            progress.cancel();
                            session.setPrefDurum(false);
                            session.setPrefID(0);
                            session.setPrefUsername("");
                            session.setPrefName("");
                            session.setPrefEposta("");
                            session.setPrefGuncellemeTarihi("");
                            session.setPrefIlce("");
                            session.setPrefKayitTarihi("");
                            session.setPrefRolID(0);
                            session.setPrefSehir("");
                            session.setPrefSifre("");
                            session.setPrefTelefon("");
                            Intent intent = new Intent(ProfilimActivity.this,MainActivity.class);
                            startActivity(intent);
                        }

                        @Override
                        public void onFailure(Call call, Throwable t) {
                            progress.cancel();
                            Toast.makeText(ProfilimActivity.this, "Hata Oluştu", Toast.LENGTH_SHORT).show();
                        }
                    });






        }
        catch (Exception e)
        {
            progress.cancel();
            Toast.makeText(this, e.getMessage().toString(), Toast.LENGTH_SHORT).show();
        }


    }


}
