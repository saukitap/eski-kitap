package com.example.user.mobilodevi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.example.user.mobilodevi.Adapters.KullaniciListAdapter;
import com.example.user.mobilodevi.Entities.APIClient;
import com.example.user.mobilodevi.Entities.Kullanicilar;
import com.example.user.mobilodevi.Services.KullanicilarService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TumKullanicilarActivity extends AppCompatActivity {


    ListView liste;
    List<Kullanicilar> list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tum_kullanicilar);




        tanimla();

        list  = new ArrayList<>();

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Bilgi");
        progressDialog.setMessage("Yükleniyor");
        progressDialog.setCancelable(false);
        progressDialog.show();


        KullanicilarService ks = APIClient.getClient().create(KullanicilarService.class);
        Call<List<Kullanicilar>> bilgiList = ks.findAll( APIClient.getApiSecurityUsername(),APIClient.getApiSecurityPass());
        bilgiList.enqueue(new Callback<List<Kullanicilar>>() {
            @Override
            public void onResponse(Call<List<Kullanicilar>> call, Response<List<Kullanicilar>> response) {

                if(response.isSuccessful())
                {
                    list=response.body();
                    KullaniciListAdapter adp = new KullaniciListAdapter(list,getApplicationContext(),TumKullanicilarActivity.this);

                    if(adp == null)
                        Toast.makeText(TumKullanicilarActivity.this, "Data Bulunamadı", Toast.LENGTH_SHORT).show();
                    else
                    {
                        liste.setAdapter(adp);


                    }
                    progressDialog.cancel();

                }
                else
                {
                    progressDialog.cancel();
                    Toast.makeText(TumKullanicilarActivity.this, response.message().toString() , Toast.LENGTH_SHORT).show();

                }



            }

            @Override
            public void onFailure(Call<List<Kullanicilar>> call, Throwable t) {

            }
        });
    }

    public void tanimla()
    {

        liste = (ListView) findViewById(R.id.listViewProducts);

    }


    public void  geriGitClick(View v)
    {
        Intent intent = new Intent(TumKullanicilarActivity.this,MainActivity.class);
        startActivity(intent);
    }
}
