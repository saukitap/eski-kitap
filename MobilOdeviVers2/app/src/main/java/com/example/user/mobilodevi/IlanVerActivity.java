package com.example.user.mobilodevi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.user.mobilodevi.Adapters.KullaniciListAdapter;
import com.example.user.mobilodevi.Entities.APIClient;
import com.example.user.mobilodevi.Entities.Kategoriler;
import com.example.user.mobilodevi.Entities.Kullanicilar;
import com.example.user.mobilodevi.Services.KategorilerService;
import com.example.user.mobilodevi.Services.KullanicilarService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IlanVerActivity extends AppCompatActivity {
    List<Kategoriler> list;
    List <String> KategoriAdlari;
    Spinner IlanVerSpinnerKategoriler , IlanVerSpinnerTurler,IlanVerSpinnerDurumlar;
    EditText IlanVerEditTextKitapAdi,IlanVerEditTextYayinEvi,IlanVerEditTextUcret;

     List<String> listecik_isim;
     List<Integer> listecik_id ;
     int kategori_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ilan_ver);


        Tanimla();

        KategorilereIstekAt();




        IlanVerSpinnerKategoriler.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    kategori_id = listecik_id.get(i);
        //Toast.makeText(IlanVerActivity.this, ""+listecik_id.get(i) , Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
});







        }

    private void KategorilereIstekAt() {


        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Bilgi");
        progressDialog.setMessage("Yükleniyor");
        progressDialog.setCancelable(false);
        progressDialog.show();


        KategorilerService ks = APIClient.getClient().create(KategorilerService.class);
        Call<List<Kategoriler>> bilgiList = ks.findAll( APIClient.getApiSecurityUsername(),APIClient.getApiSecurityPass());
        bilgiList.enqueue(new Callback<List<Kategoriler>>() {
            @Override
            public void onResponse(Call<List<Kategoriler>> call, Response<List<Kategoriler>> response) {

                if(response.isSuccessful())
                {
                    list=response.body();

                    progressDialog.cancel();

                     listecik_isim = new ArrayList<String>();
                   listecik_id = new ArrayList<Integer>();


                    for(int i=0;i<list.size();i++)
                    {
                        Kategoriler jObj = list.get(i);
                        listecik_isim.add(jObj.getKategoriAdi());
                        listecik_id.add(jObj.getID());
                        int a = 5;


                    }
                    ArrayAdapter<String> adp1 = new ArrayAdapter<String>(IlanVerActivity.this,
                            android.R.layout.simple_list_item_1, listecik_isim);
                    adp1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    IlanVerSpinnerKategoriler.setAdapter(adp1);
                    IlanVerSpinnerKategoriler.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);


                }
                else
                {
                    progressDialog.cancel();
                    // Toast.makeText(TumKullanicilarActivity.this, response.message().toString() , Toast.LENGTH_SHORT).show();

                }



            }

            @Override
            public void onFailure(Call<List<Kategoriler>> call, Throwable t) {

            }
        });



    }

    private void Tanimla() {

        IlanVerSpinnerKategoriler = findViewById(R.id.IlanVerSpinnerKategoriler);
        IlanVerSpinnerDurumlar = findViewById(R.id.IlanVerSpinnerDurumlar);
        IlanVerSpinnerTurler = findViewById(R.id.IlanVerSpinnerTurler);
        IlanVerEditTextKitapAdi=findViewById(R.id.IlanVerEditTextKitapAdi);
        IlanVerEditTextYayinEvi=findViewById(R.id.IlanVerEditTextYayinEvi);
        IlanVerEditTextUcret=findViewById(R.id.IlanVerEditTextUcret);
        list  = new ArrayList<>();
        KategoriAdlari  = new ArrayList<>();
        IlanVerSpinnerKategoriler.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

    }


    public void ilanverileri(View v)
    {
        Intent intent = new Intent(IlanVerActivity.this,IlanVer2Activity.class);
        intent.putExtra("intent_ilanver_kitap_kategori_id",""+kategori_id);
        intent.putExtra("intent_ilanver_durum",IlanVerSpinnerDurumlar.getSelectedItem().toString());
        intent.putExtra("intent_ilanver_tur_id","1");//Duzeltilecek
        intent.putExtra("intent_ilanver_kitap_adi",IlanVerEditTextKitapAdi.getText().toString());
        intent.putExtra("intent_ilanver_yayin_evi",IlanVerEditTextYayinEvi.getText().toString());
        intent.putExtra("intent_ilanver_ucret",IlanVerEditTextUcret.getText().toString());
        startActivity(intent);





    }


}
