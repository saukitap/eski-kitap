package com.example.user.mobilodevi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class InceleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incele);
    }

    public void geriClick(View v)
    {
        Intent intent = new Intent(InceleActivity.this,IlanlarimActivity.class);
        startActivity(intent);
    }
}
