package com.example.user.mobilodevi.Services;

import com.example.user.mobilodevi.Entities.Ilanlar;
import com.example.user.mobilodevi.Entities.Kategoriler;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface IlanlarService
{

    @GET("Ilanlar/findall")
    Call<List<Ilanlar>> findAll(@Header("apikey")String apikey,
                                @Header("sifre")String sifre);

    @GET("Ilanlar/find/{id}")
    Call<Ilanlar> find(@Query("id") int id  ,
                           @Header("apikey")String apikey,
                           @Header("sifre")String sifre);


    @POST("Ilanlar/create")
    Call<Void> create (@Body Ilanlar product,
                       @Header("apikey")String apikey,
                       @Header("sifre")String sifre);


    @PUT("Ilanlar/update")
    Call<Void> update (@Body Ilanlar product,
                       @Header("apikey")String apikey,
                       @Header("sifre")String sifre);

    @DELETE("Ilanlar/delete/{id}")
    Call<Void> delete(@Query("id") int id ,
                      @Header("apikey")String apikey, @Header("sifre")String sifre);






}
