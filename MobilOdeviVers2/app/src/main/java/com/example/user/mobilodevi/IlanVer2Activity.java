package com.example.user.mobilodevi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class IlanVer2Activity extends AppCompatActivity {

    String durum , kitap_adi , yayin_evi,ucret;
    Integer tur_id,kategori_id;
    EditText ilanVer2EditTextResim1,ilanVer2EditTextResim2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ilan_ver2);

      ilanVer2EditTextResim1 = findViewById(R.id.ilanVer2EditTextResim1);
      ilanVer2EditTextResim2 = findViewById(R.id.ilanVer2EditTextResim2);
        Intent intent = getIntent();
        durum = intent.getStringExtra("intent_ilanver_durum");
        kitap_adi =  intent.getStringExtra("intent_ilanver_kitap_adi");
        yayin_evi = intent.getStringExtra("intent_ilanver_yayin_evi");
        ucret  = intent.getStringExtra("intent_ilanver_ucret");
        tur_id =  Integer.parseInt(intent.getStringExtra("intent_ilanver_tur_id"));
        kategori_id = Integer.parseInt(intent.getStringExtra("intent_ilanver_kitap_kategori_id"));


    }


    public void ilanverileri2(View v)
    {

        Intent intent = new Intent(IlanVer2Activity.this,IlanVer3Activity.class);
        intent.putExtra("intent_ilanver_kitap_kategori_id",""+kategori_id);
        intent.putExtra("intent_ilanver_durum",durum);
        intent.putExtra("intent_ilanver_tur_id" ,   ""+tur_id);//Duzeltilecek
        intent.putExtra("intent_ilanver_kitap_adi",kitap_adi);
        intent.putExtra("intent_ilanver_yayin_evi",yayin_evi);
        intent.putExtra("intent_ilanver_ucret",ucret);
        intent.putExtra("intent_ilanver_resim1",ilanVer2EditTextResim1.getText().toString());
        intent.putExtra("intent_ilanver_resim2",ilanVer2EditTextResim2.getText().toString());

        startActivity(intent);

    }

    public void ilanvergeri2(View v)
    {
       // Intent intent = new Intent(IlanVer2Activity.this,IlanVerActivity.class);
      //  startActivity(intent);

    }

}
