package com.example.user.mobilodevi;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.user.mobilodevi.Adapters.ViewPagerAdapter;

public class IlanVer5Activity extends AppCompatActivity {

    ViewPager vp;
    ViewPagerAdapter vpa;
    String[] images;
    String durum , kitap_adi , yayin_evi,ucret , resim1 , resim2,adres,il,ilce,aciklama;
    Integer tur_id,kategori_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ilan_ver5);


        Intent intent = getIntent();
        resim1 = intent.getStringExtra("intent_ilanver_resim1");
        resim2 = intent.getStringExtra("intent_ilanver_resim2");
        images = new String[]{resim1,resim2};

        vp=findViewById(R.id.ilanVer5_ViewPager);
        vpa = new ViewPagerAdapter(IlanVer5Activity.this,images);
        vp.setAdapter(vpa);

        durum = intent.getStringExtra("intent_ilanver_durum");
        kitap_adi =  intent.getStringExtra("intent_ilanver_kitap_adi");
        yayin_evi = intent.getStringExtra("intent_ilanver_yayin_evi");
        ucret  = intent.getStringExtra("intent_ilanver_ucret");
        tur_id =  Integer.parseInt(intent.getStringExtra("intent_ilanver_tur_id"));
        kategori_id = Integer.parseInt(intent.getStringExtra("intent_ilanver_kitap_kategori_id"));

        adres = intent.getStringExtra("intent_ilanver_adres");
        il = intent.getStringExtra("intent_ilanver_il");
        ilce = intent.getStringExtra("intent_ilanver_ilce");
        aciklama = intent.getStringExtra("intent_ilanver_aciklama");

        Toast.makeText(this, durum + " " + kitap_adi +" " + yayin_evi +" " + ucret +" " + tur_id + " " + kategori_id +" "+resim1+" "+resim2
                +" " + adres + " " + il + " " + ilce + " " + aciklama, Toast.LENGTH_LONG).show();
      

    }

    public void ilaniyayinla(View v)
    {
        ilanVerIstek();
        Intent intent = new Intent(IlanVer5Activity.this,IlanlarimActivity.class);
        startActivity(intent);
    }

    private void ilanVerIstek() {




    }
}
